import { createConnection, ConnectionOptions } from 'typeorm';
import { configService } from '../config/config.service';
import { User } from '../model/user';
import { Item } from '../model/item.entity';
import { ItemDTO } from '../item/item.dto';
import { ItemService } from '../item/item.service';

const makeFixtures = async () => {

	const connectionOptions = { ...configService.getTypeOrmConfig(), debug: true };
	const connection = await createConnection( connectionOptions as ConnectionOptions );
	const itemService = new ItemService( connection.getRepository( Item ) );

	const generateFixtures = () => {

		const sampleSize = Array( 20 ).fill( ' ' );
		const user = new User( 'seedUser' );
		const randomNumber = () => Math.floor( Number( String( Date.now() ).slice( 0, 5 ) ) * Math.random() );

		return (
			sampleSize.map( ( _, i ) => ItemDTO.from( {
				name: `seed-${ randomNumber() }-${ i }`,
				description: 'created from seed',
				isActive: ( i % 2 ) === 0
			} )
			).map( dto => itemService
				.create( dto, user )
				.then( seed => console.log( 'done ->', JSON.stringify( seed.id ) ) )
			)
		);
	};

	return await Promise.all( generateFixtures() );
};

makeFixtures()
	.then( () => console.log( '...wait for script to exit' ) )
	.catch( ( error ) => console.error( 'seed error', error ) );
