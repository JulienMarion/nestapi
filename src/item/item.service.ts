import { Injectable } from '@nestjs/common';
import { ItemRepository } from './ItemRepository';
import { ItemDTO } from './item.dto';
import { User } from '../model/user';

@Injectable()
export class ItemService {
	constructor( private readonly repo: ItemRepository ) { }
	public async getAll (): Promise<ItemDTO[]> {
		return await this.repo
			.find()
			.then( ( items ) => items.map( ( e ) => ItemDTO.fromEntity( e ) ) );
	}

	public async find ( id: string ) {
		return await this.repo
			.findOne( id )
			.then( ( e ) => ItemDTO.fromEditableEntity( e ) )
			.catch( ( err ) => Error( err ) );
	}

	public async create ( dto: ItemDTO, user?: User ): Promise<ItemDTO> {
		if ( !user ) { user = new User( dto.createdBy ); }
		return await this.repo
			.save( ItemDTO.from( dto ).toEntity( user ) )
			.then( ( e ) => ItemDTO.fromEntity( e ) );
	}

	public async patch ( id: string, data: Partial<ItemDTO> ) {
		return await this.repo.update( id, data );
	}

	public async delete ( id: string ) {
		return await this.repo.softDelete( id );
	}
}
