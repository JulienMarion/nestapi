import {
	Column,
	CreateDateColumn,
	DeleteDateColumn,
	UpdateDateColumn,
	PrimaryGeneratedColumn,
} from 'typeorm';

export abstract class BaseEntity {
	@PrimaryGeneratedColumn( 'uuid' )
	id: string;

	@Column( { type: 'boolean', default: true } )
	isActive: boolean;

	@Column( { type: 'boolean', default: false } )
	isArchived: boolean;

	@CreateDateColumn( { type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' } )
	createdAt: Date;

	@Column( { type: 'varchar', length: 50 } )
	createdBy: string;

	@UpdateDateColumn( { type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' } )
	updatedAt: Date;

	@Column( { type: 'varchar', length: 50 } )
	updatedBy: string;

	@DeleteDateColumn()
	deletedAt: Date;
}
