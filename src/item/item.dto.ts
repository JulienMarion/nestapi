import { IsBoolean, IsDate, IsOptional, IsString, IsUUID } from 'class-validator';
import { User } from 'src/model/user';
import { Item } from '../model/item.entity';

export class ItemDTO implements Readonly<ItemDTO> {
	@IsUUID()
	id: string;

	@IsString()
	name: string;

	@IsString()
	description: string;

	@IsOptional()
	@IsDate()
	createdAt: string;

	@IsOptional()
	@IsString()
	createdBy: string;

	@IsOptional()
	@IsBoolean()
	isActive: boolean;

	public static from ( dto: Partial<ItemDTO> ) {
		const it = new ItemDTO();
		it.id = dto.id;
		it.name = dto.name;
		it.description = dto.description;
		it.createdAt = dto.createdAt;
		it.createdBy = dto.createdBy;
		it.isActive = dto.isActive;
		return it;
	}

	public static fromEntity ( entity: Item ) {
		return this.from( {
			id: entity.id,
			name: entity.name,
			description: entity.description,
			createdAt: entity.createdAt.toLocaleDateString(),
			createdBy: entity.createdBy,
			isActive: entity.isActive
		} );
	}

	public static fromEditableEntity ( entity: Item ) {
		return this.from( {
			name: entity.name,
			description: entity.description,
			isActive: entity.isActive
		} );
	}

	public toEntity ( user: User ) {
		const it = new Item();
		it.id = this.id;
		it.name = this.name;
		it.description = this.description;
		it.createdAt = new Date();
		it.updatedAt = new Date();
		it.createdBy = user ? user.name : null;
		it.updatedBy = user ? user.name : null;
		it.isActive = this.isActive;
		return it;
	}
}
