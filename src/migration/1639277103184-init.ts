import { MigrationInterface, QueryRunner } from 'typeorm';

export class init1639277103184 implements MigrationInterface {
	name = 'init1639277103184';

	public async up ( queryRunner: QueryRunner ): Promise<void> {
		await queryRunner.query(
			`CREATE TABLE "item" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "isActive" boolean NOT NULL DEFAULT true, "isArchived" boolean NOT NULL DEFAULT false, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "createdBy" character varying(50) NOT NULL, "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedBy" character varying(50) NOT NULL, "deletedAt" TIMESTAMP, "name" character varying(100) NOT NULL, "description" character varying(300) NOT NULL, CONSTRAINT "PK_d3c0c71f23e7adcf952a1d13423" PRIMARY KEY ("id"))`,
		);
	}

	public async down ( queryRunner: QueryRunner ): Promise<void> {
		await queryRunner.query( `DROP TABLE "item"` );
	}
}
