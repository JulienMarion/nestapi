
import { ItemService } from './item.service';
import { ItemDTO } from './item.dto';
import {
	Controller,
	Get,
	Post,
	Body,
	Patch,
	Param,
	Delete,
} from '@nestjs/common';

@Controller( 'items' )
export class ItemController {

	constructor( private service: ItemService ) { }

	@Get()
	public async getAll (): Promise<ItemDTO[]> {
		return await this.service.getAll();
	}

	@Get( '/:id' )
	public async find ( @Param( 'id' ) id: string ) {
		return await this.service.find( id );
	}

	@Post()
	public async post ( @Body() dto: ItemDTO ): Promise<ItemDTO> {
		return this.service.create( dto );
	}

	@Patch( '/:id' )
	public async patch ( @Param( 'id' ) id: string, @Body() data: Partial<ItemDTO> ) {
		return await this.service.patch( id, data );
	}

	@Delete( '/:id' )
	public async delete ( @Param( 'id' ) id: string ) {
		return await this.service.delete( id );
	}

}
