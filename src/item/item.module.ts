import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ItemService } from './item.service';
import { ItemController } from './item.controller';
import { ItemRepository } from './ItemRepository';

@Module( {
	imports: [ TypeOrmModule.forFeature( [ ItemRepository ] ) ],
	providers: [ ItemService ],
	controllers: [ ItemController ],
	exports: [ TypeOrmModule.forFeature( [ ItemRepository ] ), ItemService ],
} )
export class ItemModule { }
